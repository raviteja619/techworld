
from django.conf.urls import url, include
from django.contrib import admin
from apps.myapp.views import index

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^extra/', include('apps.myapp.urls')),
    url(r'^$', index, name = 'home'),
    #url(r'^how/', how, name='how'),
]
