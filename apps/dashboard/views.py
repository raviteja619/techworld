import datetime
import os
import time
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.conf import settings

def dashboard(request):
    return render(request, 'dashboard.html')