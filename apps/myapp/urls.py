from django.conf.urls import url
from django.contrib import admin
from . import views

urlpatterns = [
	url(r'^about/', views.about, name='about'),
    #url(r'^admin/', admin.site.urls),
    url(r'^services/', views.services, name='services'),
    url(r'^contact/',views.about, name='contact'),
    url(r'^team/',views.team, name='team'),
    url(r'^privacy/',views.privacy, name='privacy'),
    url(r'^terms/',views.terms, name='terms'),
    url(r'^login/',views.login, name='login'),
    url(r'^signup/',views.signup, name='signup'),
    url(r'^careers/',views.careers, name='careers'),

    
]
